﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace ListviewHW.model
{
    public class CustomClass
    {//these are the variables of the class each one is filled in when the observable collection is made
        public string DisplayName { get; set; }
        public  ImageSource Cover { get; set; }
        public string songlist { get; set; }
        public  string discription { get; set; }
        public Xamarin.Forms.Color backcolor { get; set; }
        public string yearofalbum { get; set; }
        public string funfacts { get; set; }
        public string albumtime { get; set; }
        //sources for the fun facts https://www.loudersound.com/features/anthrax-the-story-behind-every-album-cover
        //album covers found on google images
        //this function will return the filled list and this is called in the main page cs when the observable collection is made there
        public ObservableCollection<CustomClass> FillList()
        {
            ObservableCollection<CustomClass> sendlist = new ObservableCollection<CustomClass>()
            {
               new CustomClass() { DisplayName = "Fistful of Metal",yearofalbum="1984", Cover = "AnthraxFistfulOfMetal.jpg",backcolor =Xamarin.Forms.Color.FromHex("FB0E18"),
                   albumtime=" length 35:33", songlist ="Deathrider\nMetal Thrashing Mad\nI'm Eightteen\nPanic\nSubjugator\nSoldiers of Metal" +
                   "\nDeath From Above\nAnthrax\nAcross The River\nHowling Furies",discription="This album was released in 1984. This is the first Album they released" +
                   "The total length of this album is 35 minutes and 33 seconds. The album was published my Megaforce and recorded in New York",funfacts="The notorious sleeve was conceived by short-lived frontman Neil Turbin (whose homemade glove is depicted on the fist) and painted by a friend of guitarist Dan Spitz, who clinched the job when he designed the band’s iconic logo. It often ends up in ‘worst cover of all time’ lists, a status the band agreed with"
               },
               new CustomClass() { DisplayName = "Spreading the Disease",yearofalbum="1985", Cover = "Spreading.jpg",backcolor =Xamarin.Forms.Color.FromHex("FBEC0E"),
                  albumtime=" length 43:40", songlist ="A.I.R\nLone Justice\nMadhouse\nS.S.C/Stand Or Fall\nThe Enemy\nAftershock" +
                   "\nArmed And Dangerous\nMedusa\nGung-Ho",discription="This album was released in 1985. This is the second album." +
                   "The total length of the album is 43 minutes and 40 seconds.he album was published my Megaforce and recorded in New York.It was the band's first album to feature vocalist Joey Belladonna and bassist Frank Bello. ",funfacts="the cover art for Spreading The Disease was produced by designer Peter Corriston and illustrator Dave Heffernan. Ten years earlier, these men had worked on Led Zeppelin’s design classic Physical Graffiti sleeve"
               },
               new CustomClass() { DisplayName = "Among the Living",yearofalbum="1987", Cover = "Among.jpg",backcolor =Xamarin.Forms.Color.FromHex("EDFF33"),
                   albumtime=" length 50:13", songlist ="Among The Living\nCaught In A Mosh\nI Am The Law\nEfilnikuesin(N.F.L)\nA Skeleton In The Closet\nIndains" +
                   "\nOne World\nMedley:A.D.I/Horror Of It All\nImitation Of Life",discription="This album was released in 1987. This is the Third album." +
                   "The total length of the album is 50 minutes and 13 seconds.The album was published by Megaforce and was recorded in Florida and the Bahamas",funfacts="This ambiguous image of a preacher peeping out from a faceless herd always had a subtly unnerving quality, perhaps because the figure resembles the evil priest from the previous year’s Poltergeist 2."
               },
               new CustomClass() { DisplayName = "State of Euphoria",yearofalbum="1988", Cover = "state.jpg",backcolor =Xamarin.Forms.Color.FromHex("FAE306"),
                    albumtime=" length 52:35",songlist ="Be All, End All\nOut Of Sight, Out Of Mind\nMake Me Laugh\nAntisocial\nWho Cares Wins\nNow It's Dark" +
                   "\nSchism\nMisery Loves Company\n13\nFinale",discription="This album was released in 1988. It was the fourth album." +
                   "This album's length is 52 minutes and 35 seconds.This album was published by Megaforce and was recorded in Florida.",funfacts="This sleeve hasn’t got the best reputation, but the four-way logo and title - making the sleeve work whichever way up it’s held - is a neat touch."
               },
               new CustomClass() { DisplayName = "Persistance of Time",yearofalbum="1990", Cover = "AnthraxPersistenceOfTime.jpg",backcolor =Xamarin.Forms.Color.Orange,
                   albumtime=" length 58:40", songlist ="Time\nBlood\nKeep It In The Family\nIn My World\nGridlock\nInto To Reality" +
                   "\nBelly Of The Beast\nGot The Time\nH8 Red\nOne Man Stand\nDischarge",discription="This album was released in 1990. It was the Fifth Album." +
                   "The length of this album is 58 minutes and 40 seconds.This album was published by Megaforce but was recorded in California and New York.",funfacts="Don Brautigam again, here providing a complete contrast to the primary-coloured simplicity of the last sleeve with some cool, dark and complex conceptual surrealism, with aesthetic nods to The Twilight Zone opening titles and a paranoiac-critical rendering of the number 12 out of hooded figures."
               },
               new CustomClass() { DisplayName = "Sound of White Noise",yearofalbum="1993", Cover = "AnthraxSoundOfWhiteNoise.jpg",backcolor =Xamarin.Forms.Color.FromHex("FA06E3"),
                    albumtime=" length 56:56",songlist ="Potters Field\nOnly\nRoom for One More\nPackaged Rebellion\nHy Pro Glo\nInvisible" +
                   "\n1000 Points of Hate\nBlack Lodge\nC11 H17 N2 O2 S Na\nBurst\nThis is Not an Exist\nAuf Wiedersehn\n Cowboy Song\nLondon",discription="This album was released in 1993.This is the sixth album." +
                   "The total time is 56 minutes and 56 seconds. The record label that did this album was Elektra and was recorded in California. It is the band's first album to feature vocalist John Bush, who replaced longtime Anthrax vocalist Joey Belladonna in 1992.",funfacts="By 1993, metal - and Anthrax - were changing, a fact signalled by this intriguing, misty alien blur of colour and shape, apparently denoting a blob in a jar. A limited edition CD came with a skull design integrated into the blob, but the image remains enigmatically indistinct. Longtime Ministry collaborator Paul Elledge’s grainy photography of 405-line TV screens and analogue static proliferated across the band’s aesthetic in 1993, rolled out across the album inlay, two singles and an Elledge-directed video for the single Only."
               },
               new CustomClass() { DisplayName = "Stomp 442",yearofalbum="1995", Cover = "stompnew.jpg",backcolor =Xamarin.Forms.Color.FromHex("085DC5"),
                   albumtime=" length 50:56", songlist ="Random Acts of Senseless Violence\nFueled\nKing Size\nRiding Shotgun\nPerpetual Motion\nIn A Zone" +
                   "\nNothing\nAmerican Pompeii\nDrop the Ball\nTester\nBare",discription="This album was released in 1995. This is the seveth album." +
                   "The total length of this album is 50 minutes and 56 seconds.This album was under the record label Elektra and Warner. This was the lst record they recorded with Elektra.",funfacts="70s sleeve art supremo Storm Thorgerson originally designed this for Bruce Dickinson’s solo debut Balls To Picasso, but having just left Iron Maiden, Bruce couldn’t afford it (he went with a toilet wall instead). According to Bruce’s bassist Chris Dale, Thorgerson “had a quarter of a ball of scrap metal made, then rotated it with a crane and compiled the multiple shots.” It’s a compelling image, but not very Anthrax; neither’s the new logo, a ripply stoner rock font paying sad testament to a time when spikes were frowned upon. Incredibly, Walmart banned the sleeve from stores, presumably because there’s a tiny nude man’s arse on it."
               },
               new CustomClass() { DisplayName = "Volume 8:The Threat Is Real",yearofalbum="1998", Cover = "newvolume.jpg",backcolor =Xamarin.Forms.Color.FromHex("ECA20F"),
                    albumtime=" length 63:37",songlist ="Crush\nCatharsis\nInside Out\nP & V\n604\nToast to the Extras" +
                   "\nBorn Again Idiot\nKilling Box\nHarm's Way\nHog Tied\nBig Fat\nCupajoe\nAlpha Male\nStealing From a Thief/Pieces",discription="This album was released in 1998. This is the eighth album." +
                   "the total length of this album is 63 minutes and 37 seconds.The record label behind this album is Ignition and was recorded in Krusty's fun house in New York.",funfacts="The inlay folds out to reveal a War Of The Worlds-style movie poster of the band screaming at giant spider-legged robot 8-balls laying waste to a city, with soldiers on fire and explosions, which is terrific: “Every man their prisoner… Every woman their slave!”"
               },
               new CustomClass() { DisplayName = "We've Come for You All",yearofalbum="2003", Cover = "AnthraxWCFYA.jpg",backcolor =Xamarin.Forms.Color.Violet,
                  albumtime=" length 50:03",  songlist ="Contact\nWhat Doesn't Die\nSuperhero\nRefused to be Denied\nSafe Home\nAny Place but Here" +
                   "\nNobody Knows Anything\nStrap It On\nBlack Dahlia\nCaddilac Rock Box\nTaking the Music Back" +
                   "Crash\nThink About an End\nW.C.F.Y.A/We'er a Happy Family",discription="This album was released in 2003. This is the ninth album." +
                   "The length of this album is 50 minutes and 3 seconds.The record label behind this album is Sanctuary and Nuclear Blast." +
                   "This album was recorded in New York.",funfacts="The first Anthrax sleeve to be painted by comic book artist Alex Ross, who’s kept the gig as band artist right up to their latest album, even while maintaining a dizzying work rate at Marvel and DC and multiple other empires. Superman, Batman, Spider-Man, Wonder Woman, Captain America, the Incredible Hulk, Scott Ian – Ross has done them all."
               },
               new CustomClass() { DisplayName = "Worship Music",yearofalbum="2011", Cover = "worship.jpg",backcolor =Xamarin.Forms.Color.OrangeRed,
                   albumtime = "length 65:43",songlist ="Worship\nEarth on Hell\nThe Devil You Know\nFight 'Em 'Til You Can't\nI'm Aliver\nIn the End" +
                   "\nThe Giant\nJudas Priest\nCrawl\nThe Constant\nRevolution Screams",discription="This album was released in 2011. This is the tenth album." +
                   "The total length of the album is 65 minutes and 43 seconds.The record label in charge of this album is Megaforce and Nuclear Blast." ,funfacts="The tiny ‘Pentathrax’ motif at the centre of the last cover has become a shimmering golden portal, and the arms of adoring fans have become the shrivelled limbs of zombies and mutants. According to Alex Ross, the central circle was originally a turntable playing a record with the logo on it, but the band later stamped the enlarged insignia into the middle of the swarm."
               },
               new CustomClass() { DisplayName = "For All Kings",yearofalbum="2016", Cover = "ForAll.jpg",backcolor =Xamarin.Forms.Color.Yellow,
                   albumtime="59:34",songlist ="You Gotta Believe\nMonster at the End\nFor All Kings\nBreathing Lightning\nSuzerain\nEvil Twin" +
                   "\nBlood Eagle Wings\nDefend/Avenge\nAll of Them Thieves\nThis Battle Chose Us\nZero Toerance",discription="This album was released in 2016. This is the eleventh album." +
                   "This album is a total of 59 minutes and 34 seconds long. This album is under the record label Megaforce and Nuclear Blast." +
                   "It was recorded in California and New York",funfacts="“It’s beautiful, it’s grandiose, it just has this beautiful look to it,” enthused Charlie, who added that Alex Ross was “probably one of the greatest artists living.” A striking use of colour and scale, the image proved divisive among fans, the more irony-challenged of whom were concerned by its “megalomaniac aspect”."
               },

        };



            return sendlist;
        }
    }
}
