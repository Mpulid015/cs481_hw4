﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;
namespace ListviewHW
{
    public partial class MainPage : ContentPage
    {   
        //this the collection that will store the albums for the custom cells
        ObservableCollection<model.CustomClass> thelist;
        public int counter;
        public MainPage()
        {
            InitializeComponent();
           //making the list from the class
            thelist = new model.CustomClass().FillList();
            //this is so that the xaml can do the bindings for the custom cells
            Mylist.ItemsSource = thelist;
            counter = 0;
            
        }
        void MenuButton(Object sender,EventArgs e)
        {//This will show up when a cell is long pressed
            //It will display a fact about the album cover in a display alert
            var menuItem = (MenuItem)sender;
            model.CustomClass item = (model.CustomClass)menuItem.BindingContext;
            DisplayAlert("Album Fun Fact", item.DisplayName+"\n"+item.funfacts, "ok");
        }
        void RemakeList(Object sender,EventArgs e)
        {//This refreshes the list view but I just remake the whole list
           thelist = new model.CustomClass().FillList();
           Mylist.ItemsSource = thelist;
           Mylist.IsRefreshing = false;
            counter = 0;
        }
        async void Ontapped(Object sender,ItemTappedEventArgs e)
        {//this will create a new page with what cell was pressed and pass that cell into the new page for the binding
            var collect = (model.CustomClass)e.Item;
            await Navigation.PushAsync(new ChangingPage(collect));
        }
        //Adds two new albums to the collection
        void compButton(Object sender,EventArgs e)
        {
            if (counter == 0)
            {
                thelist.Add(new model.CustomClass()
                {
                    DisplayName = "Attack of the Killer B's",
                    yearofalbum = "1991",
                    songlist = "Milk\nBring the Noise\nKeep it in the Family\nStartin' up a Posse\n" +
                    "Protect and Survive\nChromatic Death\nI'm the Man\nParasite\nPipline\nSects\nBelly of the Beast\nN.F.B(Dallabnikfesin)",
                    discription = "This is a Compilation album of B- sides and covers. This was the last album released before John Bush replaced" +
                    "Joey Belladonna. Released by Megaforce.",
                    backcolor = Xamarin.Forms.Color.FromHex("D8D8D6"),
                    Cover = "Attack1.jpg",
                    funfacts = "The B's in the album's title refers to the b-sides previously unreleased and compiled for a single release",
                    albumtime = " length 44:24",
                });
                thelist.Add(new model.CustomClass()
                {
                    DisplayName = "Return of the Killer A's",
                    yearofalbum = "1999",
                    songlist = "Bring the Noise\nOnly\nPotter's Field\nBall of Confusion\n" +
                    "Crush\nRoom for One More\nInside Out\nHy Pro Glo\nFueled\nAmong the Living\nGot the Time\n" +
                    "Indians\nAntisocial\nI'm the Man\nMadhouse\nI Am the Law\nMetal Thrashing Mad",
                    discription = "This album contains at least one song from each of their albums up to the release of this album in 1999.",
                    backcolor = Xamarin.Forms.Color.FromHex("CACAC9"),
                    Cover = "Returnnew.jpg",
                    funfacts = "The cover of 'Ball of Confusion' features Anthrax's first new recording with former lead singer Joey Belladonna since 1991, and their first with former bassist Dan Lilker since 1984. ",
                    albumtime = " length 74:50",
                });
                counter = 1;
            }
        }
    }
}
