﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListviewHW
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChangingPage : ContentPage
	{
		public ChangingPage (model.CustomClass theitem)
		{//This will do the binding based on what cell was chosen
            //This way there isn't a need to make the different pages for each one
			InitializeComponent ();
            Thedisplay.Text =theitem.DisplayName;
            Thecover.Source =theitem.Cover;
            TheLayout.BackgroundColor = theitem.backcolor;
            SongList.Text = theitem.songlist;
            Thediscription.Text = theitem.discription;
		}
	}
}